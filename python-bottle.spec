Name:           python-bottle
Version:        0.13.2
Release:        1
Summary:        WSGI micro web-framework for Python.
License:        MIT
URL:            https://github.com/bottlepy/bottle
Source0:        https://github.com/bottlepy/bottle/archive/%{version}/bottle-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel python3-setuptools
BuildRequires:  python3dist(pytest)

%description
Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
It is distributed as a single file module and has no dependencies other than
the Python Standard Library.

%package        -n python3-bottle
Summary:        WSGI micro web-framework for Python.
%{?python_provide:%python_provide python%{python3_pkgversion}-bottle}

%description    -n python3-bottle
Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
It is distributed as a single file module and has no dependencies other than
the Python Standard Library.

%prep
%autosetup -n bottle-%{version} -p1
sed -i '/^#!/d' bottle.py

%build
%py3_build

%install
%py3_install
rm %{buildroot}%{_bindir}/bottle

%check
%{pytest}

%files -n python3-bottle
%license LICENSE
%doc AUTHORS README.rst
%{python3_sitelib}/*
%exclude %{_bindir}/bottle.py

%changelog
* Tue Nov 05 2024 ZhaoYu Jiang <jiangzhaoyu@kylinos.cn> - 0.13.2-1
- Upgrade Package to Version 0.13.2
  Signed cookies now use a stronger HMAC algorithm by default.
  Bottle now ships with its own multipart form data parser (borrowed from multipart) and no longer relies on cgi.FieldStorage.

* Fri May 26 2023 wulei <wu_lei@hoperun.com> - 0.12.25-1
- Upgrade package to version 0.12.25

* Tue Jun 14 2022 yaoxin <yaoxin30@h-partners.com> - 0.12.13-11
- Fix CVE-2022-31799

* Thu Mar 31 2022 xu_ping <xuping33@huawei.com> - 0.12.13-10
- Fix Python 3.7 collections.abc DeprecationWarning

* Fri Feb 19 2021 zhanghua <zhanghua40@huawei.com> - 0.12.13-9
- fix CVE-2020-28473

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 0.12.13-8
- Modify url and remove subpackage python2-bottle

* Tue Nov 26 2019 zhujunhao <zhujunhao5@huawei.com> - 0.12.13-7
- Package init
